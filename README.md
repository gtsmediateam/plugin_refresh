Execute function once per page load, setting timeout. Default timeout is 5 seconds.

```javascript
mmcore.oncePerPageLoad('say1', function () {say(1)});

// 7 seconds as a timeout.
mmcore.oncePerPageLoad('say2', function () {say(2)}, 7000);
```

### TODO

Add best practices of usage.
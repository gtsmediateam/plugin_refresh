/**
 * Prevent function execution on page refresh.
 * 24.12.2014
 * Alexander Tytskiy
 */

/**
 * Refresh plugin.
 * Exports mmcore.oncePerPageLoad() function.
 *
 * Execute function once per page load. Rule user left page defined by timeout in milliseconds.
 *
 * @author   gts media team
 * @version  0.1.1
 *
 * @param verbalID {String}   Verbal identifier to store our function as done
 * @param callback {Function} Function to execute once per page load
 * @param timeout  {Number}   Timeout in milliseconds (5000 per default)
 *
 * Example:
 *     mmcore.oncePerPageLoad('say1', function () {say(1)});
 *     mmcore.oncePerPageLoad('say2', function () {say(2)}, 7000);
 */
(function () {
	/* jshint strict: false */
	/* jshint maxlen: false */
	var mmcore = window.mmcore,

		errorPlaceholder = null,

		SECOND = 1000,
		JSON_ERROR = 'JSON is not available in this browser.';

	var getCurrentTimestamp = function () {
		return new Date().getTime();
	};

	var manager = {
		_stack: [],
		_preventeds: {},

		_timestamp: getCurrentTimestamp(),
		_timeout: 5 * SECOND,
		_oldOnunloadFn: window.onunload,
		_isLocalStorageAvailable: false,

		_storage: window.localStorage,
		_keyName: 'mm_prevent_f5',

		getCurrentTimestamp: getCurrentTimestamp,

		// Main API.
		oncePerPageLoad: function (verbalID, callback, timeout) {
			var item;

			verbalID = 'fn_' + verbalID;
			timeout = timeout || 0;

			if (!manager._preventeds[verbalID]) {
				callback.call(null);
			}

			item = manager._constructItem(verbalID, timeout);

			manager._stack.push(item);

			return manager;
		},

		initialize: function () {
			manager._isLocalStorageAvailable = manager.testLocalStorage();

			manager
				._fillPreventeds()
				._cleanExpired()
				._bindEvents();
		},

		testLocalStorage: function () {
			var key = 'mm_test';

			try {
				manager._storage.setItem(key, key);
				manager._storage.removeItem(key);
				return true;
			} catch (error) {
				return false;
			}
		},

		isEmptyObj: function (obj) {
			for (var prop in obj) {
				if (obj.hasOwnProperty(prop)) {
					return false;
				}
			}

			return true;
		},

		_fillPreventeds: function () {
			try {
				if (manager._isLocalStorageAvailable) {
					manager._preventeds = JSON.parse(manager._storage.getItem(manager._keyName)) || {};
				} else {
					manager._preventeds = JSON.parse(mmcore.GetCookie(manager._keyName, true)) || {};
				}
			} catch (err) {
				manager._preventeds = {};
			}

			return manager;
		},

		_bindEvents: function () {
			window.onunload = function () {
				var splitten;

				for (var i = 0, len = manager._stack.length; i < len; i++) {
					splitten = manager._stack[i].split(':');
					splitten[1] = parseInt(splitten[1], 10);

					if (splitten[1]) {
						manager._preventeds[splitten[0]] = {
							timestamp: manager.getCurrentTimestamp(),
							timeout: splitten[1]
						};
					} else {
						manager._preventeds[splitten[0]] = {
							timestamp: manager.getCurrentTimestamp()
						};
					}
				}

				if (manager.isEmptyObj(manager._preventeds)) {
					if (manager._isLocalStorageAvailable) {
						manager._storage.removeItem(manager._keyName);
					} else {
						mmcore.SetCookie(manager._keyName, '', -1, true);
					}
				} else {
					if (manager._isLocalStorageAvailable) {
						manager._storage.setItem(manager._keyName, JSON.stringify(manager._preventeds));
					} else {
						mmcore.SetCookie(manager._keyName, JSON.stringify(manager._preventeds), 0, true);
					}
				}

				if (manager._oldOnunloadFn) {
					return manager._oldOnunloadFn.apply(this, arguments);
				}
			};

			return manager;
		},

		_cleanExpired: function () {
			var isExpired = false,
				timeout = 0;

			for (var prop in manager._preventeds) {
				if (manager._preventeds.hasOwnProperty(prop)) {
					timeout = manager._preventeds[prop].timeout || manager._timeout;
					isExpired = Boolean((+ manager._preventeds[prop].timestamp + timeout < manager._timestamp));

					if (isExpired) {
						delete manager._preventeds[prop];
					}
				}
			}

			return manager;
		},

		_constructItem: function (id, timestamp) {
			return (id + ':' + timestamp);
		},

		// Debug.
		_cleanRefreshManager: function () {
			manager._preventeds = {};
			manager._stack = [];

			if (manager._isLocalStorageAvailable) {
				manager._storage.removeItem(manager._keyName);
			} else {
				mmcore.SetCookie(manager._keyName, '', -1, true);
			}
		}
	};

	function fallback(verbalID, callback) {
		callback.call(null);
		mmcore.EH(new Error('Plugin_Refresh.js: something went wrong. Executed prevented function. Reason: ' + (errorPlaceholder || JSON_ERROR)));
	}

	if (!window.JSON) {
		// Anyway execute callback if there is no JSON in browser.
		mmcore.oncePerPageLoad = fallback;

		return;
	}

	try {
		manager.initialize();

		mmcore.oncePerPageLoad = manager.oncePerPageLoad;

		// Export this only for debugging.
		mmcore._cleanRefreshManager = manager._cleanRefreshManager;
	} catch (error) {
		errorPlaceholder = error;

		// Anyway execute callback if something broken.
		mmcore.oncePerPageLoad = fallback;
	}
}());
